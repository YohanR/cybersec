var router = require('express').Router()
var vulnDict = require('../config/vulns')
var authHandler = require('../core/authHandler')
var LocalStrategy = require('passport-local').Strategy
var passport = require('passport')
router.use(passport.initialize());
router.use(passport.session());
const AccessToken = require("../models/user");



/*passport.use('authtoken', new LocalStrategy(
	function(token, done) {
		AccessToken.findOne({
		id: token
	  }, function(error, accessToken) {
		if (error) {
		  return done(error);
		}
  
		if (accessToken) {
		  if (!token.isValid(accessToken)) {
			return done(null, false);
		  }
  
		  User.findOne({
			id: accessToken.userId
		  }, function(error, user) {
			if (error) {
			  return done(error);
			}
  
			if (!user) {
			  return done(null, false);
			}
  
			return done(null, user);
		  });
		} else {
		  return done(null);
		}
	  });
	}
  ));
*/
module.exports = function (passport) {
	router.get('/', authHandler.isAuthenticated, function (req, res) {
		res.redirect('/learn')
	})

	router.get('/login', authHandler.isNotAuthenticated, function (req, res) {
		res.render('login')
	})

	router.get('/learn/vulnerability/:vuln', authHandler.isAuthenticated, function (req, res) {
		res.render('vulnerabilities/layout', {
			vuln: req.params.vuln,
			vuln_title: vulnDict[req.params.vuln],
			vuln_scenario: req.params.vuln + '/scenario',
			vuln_description: req.params.vuln + '/description',
			vuln_reference: req.params.vuln + '/reference',
			vulnerabilities:vulnDict
		}, function (err, html) {
			if (err) {
				console.log(err)
				res.status(404).send('404')
			} else {
				res.send(html)
			}
		})
	})

	router.get('/learn', authHandler.isAuthenticated, function (req, res) {
		res.render('learn',{vulnerabilities:vulnDict})
	})

	router.get('/register', authHandler.isNotAuthenticated, function (req, res) {
		res.render('register')
	})

	router.get('/logout', function (req, res) {
		req.logout();
		res.redirect('/');
	})

	router.get('/forgotpw', function (req, res) {
		res.render('forgotpw')
	})

	router.get('/resetpw', authHandler.resetPw)

	router.post('/login', passport.authenticate('login', {
		successRedirect: '/learn',
		failureRedirect: '/login',
		failureFlash: true
	}))



	/*router.post('/login',
	passport.authenticate(
	  'authtoken',
	  {
		session: false,
		optional: false
	  }
	),
	function(req, res) {
	  res.redirect('/');
	}
  );*/

	router.post('/register', passport.authenticate('signup', {
		successRedirect: '/learn',
		failureRedirect: '/register',
		failureFlash: true
	}))

	router.post('/forgotpw', authHandler.forgotPw)

	router.post('/resetpw', authHandler.resetPwSubmit)

	return router
}