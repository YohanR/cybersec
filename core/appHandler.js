var db = require('../models')
const md5 = require('md5');
const exec = require('child_process').execFile;
var mathjs = require('mathjs')
var libxmljs = require("libxmljs");
const { QueryTypes } = require('sequelize');
var sanitizer = require('sanitizer');
var serialize = require("node-serialize")
const Op = db.Sequelize.Op


// pour se protéger des failles sql, on echap tous les codes executables venant de l'input avec le replacement
module.exports.userSearch = function (req, res) {
	var query = "SELECT name,id FROM Users WHERE login=:login";
	db.sequelize.query(query, {
		model: db.User,
		replacements : {login : req.body.login}
	}).then(user => {
		if (user.length) {
			var output = {
				user: {
					name: user[0].name,
					id: user[0].id
				}
			}
			res.render('app/usersearch', {
				output: sanitizer.escape(output)
			})
		} else {
			req.flash('warning', 'User not found')
			res.render('app/usersearch', {
				output: null
			})
		}
	}).catch(err => {
		req.flash('danger', 'Internal Error')
		res.render('app/usersearch', {
			output: null
		})
	})
}

module.exports.ping = function (req, res) {
    const ping = 'ping'
    const arguments = ['-c','2', req.body.address]
    exec(ping, arguments, function (err, output) {
        if(!err) {
        res.render('app/ping', {
            output: output})
            return
        }else{
            res.status(500).send(err)
            return
        }
        })
    }

module.exports.listProducts = function (req, res) {
	db.Product.findAll().then(products => {
		output = {
			products: products
		}
		res.render('app/products', {
			replacements : {login : req.body.login}
		})
	})
}

module.exports.productSearch = function (req, res) {
	db.Product.findAll({
		where: {
			name: {
				[Op.like]: '%' + req.body.name + '%'
			}
		}
	}).then(products => {
		output = {
			products: products,
			searchTerm: req.body.name
		}
		res.render('app/products', {
			output: sanitizer.escape(output)
		})
	})
}

module.exports.modifyProduct = function (req, res) {
	if (!req.query.id || req.query.id == '') {
		output = {
			product: {}
		}
		res.render('app/modifyproduct', {
			output: output
		})
	} else {
		db.Product.find({
			where: {
				'id': req.query.id
			}
		}).then(product => {
			if (!product) {
				product = {}
			}
			output = {
				product: product
			}
			res.render('app/modifyproduct', {
				output: output
			})
		})
	}
}

module.exports.modifyProductSubmit = function (req, res) {
	if (!req.body.id || req.body.id == '') {
		req.body.id = 0
	}
	db.Product.find({
		where: {
			'id': req.body.id
		}
	}).then(product => {
		if (!product) {
			product = new db.Product()
		}
		// Utilisation de sanitizer pour echaper toutes les entrées "html".
		product.code = sanitizer.escape(req.body.code)
		product.name =  sanitizer.escape(req.body.name)
		product.description =  sanitizer.escape(req.body.description)
		product.tags =  sanitizer.escape(req.body.tags)
		product.save().then(p => {
			if (p) {
				req.flash('success', 'Product added/modified!')
				res.redirect('/app/products')
			}
		}).catch(err => {
			output = {
				product: product
			}
			req.flash('danger',err)
			res.render('app/modifyproduct', {
				output: output
			})
		})
	})
}

module.exports.userEdit = function (req, res) {
	res.render('app/useredit', {
		userId: req.user.id,
		userEmail: req.user.email,
		userName: req.user.name
	})
}

module.exports.userEditSubmit = function (req, res) {
	db.User.find({
		where: {
			'id': req.body.id
		}		
	}).then(user =>{
		if(req.body.password.length>0){
			if(req.body.password.length>0){
				if (req.body.password == req.body.cpassword) {
					user.password = md5(sanitizer.escape(req.body.password))
				}else{
					req.flash('warning', 'Passwords dont match')
					res.render('app/useredit', {
						userId: sanitizer.escape(req.user.id),
						userEmail: sanitizer.escape(req.user.email),
						userName: sanitizer.escape(req.user.name),
					})
					return		
				}
			}else{
				req.flash('warning', 'Invalid Password')
				res.render('app/useredit', {
					userId: sanitizer.escape(req.user.id),
					userEmail: sanitizer.escape(req.user.email),
					userName: sanitizer.escape(req.user.name),
				})
				return
			}
		}
		user.email = sanitizer.escape(req.body.email)
		user.name = sanitizer.escape(req.body.name)
		user.save().then(function () {
			req.flash('success',"Updated successfully")
			res.render('app/useredit', {
				userId: sanitizer.escape(req.body.id),
				userEmail: sanitizer.escape(req.body.email),
				userName: sanitizer.escape(req.body.name),
			})
		})
	})
}

module.exports.redirect = function (req, res) {
	if (req.query.url) {
		res.redirect(req.query.url)
	} else {
		res.send('invalid redirect url')
	}
}

module.exports.calc = function (req, res) {
	if (req.body.eqn) {
		res.render('app/calc', {
			output: mathjs.eval(req.body.eqn)
		})
	} else {
		res.render('app/calc', {
			output: 'Enter a valid math string like (3+3)*2'
		})
	}
}

module.exports.listUsersAPI = function (req, res) {
	db.User.findAll({}).then(users => {
		res.status(200).json({
			success: true,
			users: users
		})
	})
}

module.exports.bulkProductsLegacy = function (req,res){
	// TODO: Deprecate this soon
	if(req.files.products){
		var products = serialize.unserialize(req.files.products.data.toString('utf8'))
		products.forEach( function (product) {
			var newProduct = new db.Product()
			newProduct.name = product.name
			newProduct.code = product.code
			newProduct.tags = product.tags
			newProduct.description = product.description
			newProduct.save()
		})
		res.redirect('/app/products')
	}else{
		res.render('app/bulkproducts',{messages:{danger:'Invalid file'},legacy:true})
	}
}

module.exports.bulkProducts =  function(req, res) {
	if (req.files.products && req.files.products.mimetype=='text/xml'){
		var products = libxmljs.parseXmlString(req.files.products.data.toString('utf8'), {noent:true,noblanks:true})
		products.root().childNodes().forEach( product => {
			var newProduct = new db.Product()
			newProduct.name = product.childNodes()[0].text()
			newProduct.code = product.childNodes()[1].text()
			newProduct.tags = product.childNodes()[2].text()
			newProduct.description = product.childNodes()[3].text()
			newProduct.save()
		})
		res.redirect('/app/products')
	}else{
		res.render('app/bulkproducts',{messages:{danger:'Invalid file'},legacy:false})
	}
}
